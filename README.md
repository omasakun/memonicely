# Memonicely
[memonicely](https://github.com/omasakun/memonicely) makes remembering things easy. It implements an adaptive scheduling algorithm to maximize the learning efficient.

You can make your own flashcards on Google Sheets.

<p align="center">
	<a href="https://omasakun.github.io/memonicely"><b>Try it Now!</b></a>
</p>

<!--
// TODO: Add screenshot
<p align="center">
	<img alt="Memo Nicely Preview" src="preview.png">
</p>
-->

## Repositories
- [GitHub](https://github.com/omasakun/memonicely)
- [GitLab](https://gitlab.com/omasakun/memonicely)

## Directory Structure
- [.vscode](.vscode) : [VSCode](https://code.visualstudio.com/) settings
- [src](src) : The app source
- [docs](docs) : The compiled app

## How to compile
1. `pnpm install` or `npm install` : Install necessary npm packages
2. `npm run clean` : Delete unnecessary files.
3. `npm run tsc` : Compile TypeScript
4. `npm run dev` : Compile Sass/Pug & Bundle JavaScript & Launch live server

## License

Copyright (c) 2019 [omasakun](https://github.com/omasakun)

Licensed under the [MIT](LICENSE) License.
